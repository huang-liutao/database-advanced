``` mysql
/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2023-09-22 09:09:19                          */
/*==============================================================*/
create database tt charset utf8;
use tt;

drop table if exists attributes;

drop table if exists attributes_values;

drop table if exists rele;

drop table if exists sku;

drop table if exists spu;

/*==============================================================*/
/* Table: attributes                                            */
/*==============================================================*/
create table attributes
(
   attributes_id        int not null auto_increment,
   attributes_name      varchar(10) not null,
   primary key (attributes_id)
);

/*==============================================================*/
/* Table: attributes_values                                     */
/*==============================================================*/
create table attributes_values
(
   values_id            int not null auto_increment,
   values_test          varchar(20) not null,
   primary key (values_id)
);

/*==============================================================*/
/* Table: rele                                                  */
/*==============================================================*/
create table rele
(
   rele_id              int not null auto_increment,
   sku_id               int,
   attributes_id        int,
   values_id            int,
   primary key (rele_id)
);

/*==============================================================*/
/* Table: sku                                                   */
/*==============================================================*/
create table sku
(
   sku_id               int not null auto_increment,
   spu_id               int,
   sku_name             varchar(50) not null,
   sku_price            decimal(6,2) not null,
   sku_add              varchar(20) not null,
   primary key (sku_id)
);

/*==============================================================*/
/* Table: spu                                                   */
/*==============================================================*/
create table spu
(
   spu_id               int not null auto_increment,
   spu_name             varchar(20) not null,
   spu_text             varchar(255) not null,
   primary key (spu_id)
);

alter table rele add constraint FK_attributes_rele foreign key (attributes_id)
      references attributes (attributes_id) on delete restrict on update restrict;

alter table rele add constraint FK_sku_rele foreign key (sku_id)
      references sku (sku_id) on delete restrict on update restrict;

alter table rele add constraint FK_values_rele foreign key (values_id)
      references attributes_values (values_id) on delete restrict on update restrict;

alter table sku add constraint FK_spu_sku foreign key (spu_id)
      references spu (spu_id) on delete restrict on update restrict;

# 查询1
select a.spu_id,b.sku_id,b.sku_name,b.sku_price,b.sku_add
 from spu a,sku b,attributes c,attributes_values d,rele e 
where 
	a.spu_id = b.spu_id
	and b.sku_id = e.sku_id
	and c.attributes_id = e.attributes_id
	and d.values_id = e.values_id ;
	
# 查询2
select a.spu_id,b.sku_id,b.sku_name,b.sku_price,b.sku_add
from spu a,sku b,attributes c,attributes_values d,rele e 
where 
	a.spu_id = b.spu_id
	and b.sku_id = e.sku_id
	and c.attributes_id = e.attributes_id
	and d.values_id = e.values_id 
and b.sku_id = 
(
select aa.sku_id from 
(select sku_id from attributes, attributes_values , rele where attributes.attributes_id =rele.attributes_id and attributes_values.values_id = rele.values_id and values_test = "黑猪" ) aa
,
(select sku_id from attributes, attributes_values , rele where attributes.attributes_id =rele.attributes_id and attributes_values.values_id = rele.values_id and values_test = "200g" ) bb
,
(select sku_id from attributes, attributes_values , rele where attributes.attributes_id =rele.attributes_id and attributes_values.values_id = rele.values_id and values_test = "1包" ) cc
where aa.sku_id = bb.sku_id
and bb.sku_id = cc.sku_id
);

```

![image-20230924163547326](https://s2.loli.net/2023/09/24/Z2dPCAgzaEhxJ9D.png)

![image-20230924163617602](https://s2.loli.net/2023/09/24/skBJgn37QxUcXWV.png)

![image-20230924163634393](https://s2.loli.net/2023/09/24/YIxF2dmAqj751Zl.png)

